package com.tsc.yandexsearchstatistics.service;

import com.tsc.yandexsearchstatistics.model.Domain;

import java.util.List;
import java.util.Optional;

public interface DomainService {

    void create(Domain domain);

    List<Domain> findAll();

    Optional<Domain> read(Domain domain);

    boolean update(Domain domain);

    boolean delete(int id);

}
