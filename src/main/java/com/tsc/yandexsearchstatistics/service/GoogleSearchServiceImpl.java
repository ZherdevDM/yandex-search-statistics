package com.tsc.yandexsearchstatistics.service;

import com.tsc.yandexsearchstatistics.model.Domain;
import com.tsc.yandexsearchstatistics.model.QueryLists;
import com.tsc.yandexsearchstatistics.model.QueryTag;
import com.tsc.yandexsearchstatistics.model.SearchEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;

import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static org.yaml.snakeyaml.util.UriEncoder.decode;


@Service
public class GoogleSearchServiceImpl implements GoogleSearchService {

    private final QueryTagServiceImpl queryTagService;

    @Autowired
    public GoogleSearchServiceImpl(QueryTagServiceImpl queryTagService) {
        this.queryTagService = queryTagService;
    }

    @Override
    public List<Domain> getDomainList(List<String> query) {
        List<Domain> domains = new ArrayList<>();
        QueryLists queryLists = processQuery(query);

        for (QueryTag queryTag : queryLists.getUniqueTags()) {
            requestGoogleSearch(queryTag);
        }
        return domains;
    }


    private QueryLists processQuery(List<String> queryTags) {
        QueryLists query = new QueryLists();

        for (String tag : queryTags) {
            final QueryTag queryTag;

            if (tag != null && !tag.equals("")) {
                queryTag = new QueryTag(tag);
                if (queryTagService.checkExistence(queryTag)) {
                    queryTagService.create(queryTag);
                    query.addToUniqueTags(queryTag);
                } else query.addToExistedTags(queryTag);
            }
        }
        return query;
    }

    private SearchEvent requestGoogleSearch(QueryTag queryTag) {

        try {
            String urlProgrammableSearch = "https://www.googleapis.com/customsearch/v1?key=AIzaSyB-PMUzBTgNiqFo7Br7sQ5paX1MJ6oFcHQ&cx=31839ca55c47e43e1q=";
            URL url = new URL(urlProgrammableSearch + queryTag.getName());
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            InputStream response = connection.getInputStream();


            if (connection.getResponseMessage() != null && !connection.getResponseMessage().isEmpty()) {

            }
        } catch (IOException e) {
            e.printStackTrace();
        }


        return null;
    }

}
