package com.tsc.yandexsearchstatistics.service;

import com.tsc.yandexsearchstatistics.model.Domain;
import com.tsc.yandexsearchstatistics.repository.DomainRepository;
import lombok.var;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class DomainServiceImpl implements DomainService{

    @Autowired
    private DomainRepository repository;

    @Override
    public void create(Domain domain) {
        repository.save(domain);
    }

    @Override
    public List<Domain> findAll() {
        var domain = (List<Domain>) repository.findAll();

        return domain;
    }

    @Override
    public Optional<Domain> read(Domain domain) {
        return repository.findById(domain.getId());
    }

    @Override
    public boolean update(Domain domain) {
        try {
            repository.save(domain);
        } catch (Exception exception) {
            return true;
        }

        return true;
    }

    @Override
    public boolean delete(int id) {
        try {
            repository.deleteById(id);
        } catch (Exception exception) {
            return true;
        }

        return true;
    }
}
