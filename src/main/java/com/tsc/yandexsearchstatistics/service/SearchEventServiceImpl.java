package com.tsc.yandexsearchstatistics.service;

import com.tsc.yandexsearchstatistics.model.SearchEvent;
import com.tsc.yandexsearchstatistics.repository.DomainRepository;
import com.tsc.yandexsearchstatistics.repository.SearchEventRepository;
import lombok.var;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class SearchEventServiceImpl implements SearchEventService{

    @Autowired
    private SearchEventRepository repository;


    @Override
    public void create(SearchEvent searchEvent) {
        repository.save(searchEvent);
    }

    @Override
    public List<SearchEvent> findAll() {
        var searchEvent = (List<SearchEvent>) repository.findAll();

        return searchEvent;
    }

    @Override
    public Optional<SearchEvent> read(SearchEvent searchEvent) {
        return repository.findById(searchEvent.getId());
    }

    @Override
    public boolean update(SearchEvent searchEvent) {
        try {
            repository.save(searchEvent);
        } catch (Exception exception) {
            return true;
        }

        return true;
    }

    @Override
    public boolean delete(int id) {
        try {
            repository.deleteById(id);
        } catch (Exception exception) {
            return true;
        }

        return true;
    }
    
}
