package com.tsc.yandexsearchstatistics.controller;

import com.tsc.yandexsearchstatistics.model.Domain;
import com.tsc.yandexsearchstatistics.model.SearchEvent;
import com.tsc.yandexsearchstatistics.service.DomainServiceImpl;
import com.tsc.yandexsearchstatistics.service.GoogleSearchServiceImpl;
import com.tsc.yandexsearchstatistics.service.SearchEventServiceImpl;
import lombok.var;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.Map;

@RestController
public class GoogleSearchController {

    private final GoogleSearchServiceImpl googleSearchService;

    @Autowired
    public GoogleSearchController(GoogleSearchServiceImpl googleSearchService) {
        this.googleSearchService = googleSearchService;
    }

    @GetMapping(value = "/search")
    public ResponseEntity<List<Domain>> getDomainsFromGoogle(@RequestParam(name = "query") List<String> query) {

        final List<Domain> domains = googleSearchService.getDomainList(query);

        return domains != null &&  !domains.isEmpty()
                ? new ResponseEntity<>(domains, HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }


}
