package com.tsc.yandexsearchstatistics.repository;

import com.tsc.yandexsearchstatistics.model.LogEvent;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LogEventRepository extends CrudRepository<LogEvent, Integer> {
}