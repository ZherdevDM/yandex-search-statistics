package com.tsc.yandexsearchstatistics.model;

import lombok.NonNull;
import org.apache.kafka.common.protocol.types.Field;

import javax.persistence.*;
import java.net.URL;
import java.sql.Timestamp;
import java.util.Objects;

    @Entity
    @Table(name = "SearchEvent")
    public class SearchEvent {

        @Id
        @Column(name = "id")
        @GeneratedValue(strategy = GenerationType.AUTO)
        private Integer id;

        @NonNull
        @Column(name = "searchUrl")
        private String searchUrl;

        @NonNull
        @Column(name = "timeDate")
        private Timestamp timeDate;

        public SearchEvent(String searchUrl, Timestamp timeDate) {
            this.searchUrl = searchUrl;
            this.timeDate = timeDate;
        }

        public Integer getId() {
            return id;
        }

        public String getName() {
            return searchUrl;
        }

        public void setName(String searchUrl) {
            this.searchUrl = searchUrl;
        }

        @Override
        public int hashCode() {
            int hash = 7;
            hash = 79 * hash + Objects.hashCode(this.id);
            hash = 79 * hash + Objects.hashCode(this.searchUrl);
            hash = 79 * hash + Objects.hashCode(this.timeDate);
            return hash;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            final com.tsc.yandexsearchstatistics.model.SearchEvent other = (com.tsc.yandexsearchstatistics.model.SearchEvent) obj;

            if (!Objects.equals(this.searchUrl, other.searchUrl) && !Objects.equals(this.timeDate, other.timeDate)) {
                return false;
            }
            return Objects.equals(this.id, other.id);
        }

        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder("SearchEvent{");
            sb.append("id=").append(id);
            sb.append(", searchUrl='").append(searchUrl).append('\'');
            sb.append(", timeDate='").append(timeDate).append('\'');
            sb.append('}');
            return sb.toString();
        }
    }