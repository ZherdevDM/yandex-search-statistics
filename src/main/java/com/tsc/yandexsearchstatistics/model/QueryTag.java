package com.tsc.yandexsearchstatistics.model;

import lombok.NonNull;

import javax.persistence.*;
import java.util.Objects;

    @Entity
    @Table(name = "QueryTag")
    public class QueryTag {

        @Id
        @Column(name = "id")
        @GeneratedValue(strategy = GenerationType.AUTO)
        private Integer id;

        @NonNull
        @Column(name = "name")
        private String name;

        public QueryTag() {
        }

        public QueryTag(int id, String name) {

            this.id = id;
            this.name = name;
        }

        public QueryTag(String name) {

            this.name = name;
        }


        public Integer getId() {
            return id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        @Override
        public int hashCode() {
            int hash = 7;
            hash = 79 * hash + Objects.hashCode(this.id);
            hash = 79 * hash + Objects.hashCode(this.name);
            return hash;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            final com.tsc.yandexsearchstatistics.model.QueryTag other = (com.tsc.yandexsearchstatistics.model.QueryTag) obj;

            if (!Objects.equals(this.name, other.name)) {
                return false;
            }
            return Objects.equals(this.id, other.id);
        }

        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder("QueryTag{");
            sb.append("id=").append(id);
            sb.append(", name='").append(name).append('\'');
            sb.append('}');
            return sb.toString();
        }
    }
