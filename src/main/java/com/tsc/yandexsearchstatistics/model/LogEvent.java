package com.tsc.yandexsearchstatistics.model;

import lombok.NonNull;

import javax.persistence.*;
import java.util.Objects;

    @Entity
    @Table(name = "LogEvent")
    public class LogEvent {

        @Id
        @Column(name = "id")
        @GeneratedValue(strategy = GenerationType.AUTO)
        private Integer id;

        @NonNull
        @Column(name = "domain_id")
        private Integer domain_id;

        @NonNull
        @Column(name = "querytag_id")
        private Integer querytag_id;

        @NonNull
        @Column(name = "searchivent_id")
        private Integer searchivent_id;

        public LogEvent() {
        }

        public LogEvent(int id, Integer domain_id, Integer searchivent_id, Integer querytag_id) {

            this.id = id;
            this.domain_id = domain_id;
            this.searchivent_id = searchivent_id;
            this.querytag_id = querytag_id;
        }


        public Integer getId() {
            return id;
        }


        @Override
        public int hashCode() {
            int hash = 7;
            hash = 79 * hash + Objects.hashCode(this.id);
            hash = 79 * hash + Objects.hashCode(this.domain_id);
            hash = 79 * hash + Objects.hashCode(this.querytag_id);
            hash = 79 * hash + Objects.hashCode(this.searchivent_id);
            return hash;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            final com.tsc.yandexsearchstatistics.model.LogEvent other = (com.tsc.yandexsearchstatistics.model.LogEvent) obj;

            if (!Objects.equals(this.domain_id, other.domain_id)
                    && !Objects.equals(this.searchivent_id, other.searchivent_id)
                    && !Objects.equals(this.querytag_id, other.querytag_id)) {
                return false;
            }
            return Objects.equals(this.id, other.id);
        }

        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder("LogEvent{");
            sb.append("id=").append(id);
            sb.append(", domain_id='").append(domain_id).append('\'');
            sb.append(", searchivent_id='").append(searchivent_id).append('\'');
            sb.append(", querytag_id='").append(querytag_id).append('\'');
            sb.append('}');
            return sb.toString();
        }
    }
